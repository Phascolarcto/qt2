#include <QDebug>
#include <QCoreApplication>
#include <iostream>
#include <string>
#include <core/core.hpp>
#include <highgui/highgui.hpp>
#include <boost/program_options.hpp>
#include <features2d/features2d.hpp>
#include <cvaux.h>
#include <cv.h>
#include <direct.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <clocale>
#include <boost/log/utility/setup.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <ctime>
using namespace cv;
using namespace std;
using namespace boost::program_options;
bool checkInputDir (string input_dir)
{
    try
    {
        boost::filesystem::recursive_directory_iterator rdib(input_dir);
    }
    catch (...)
    {
        BOOST_LOG_TRIVIAL(error)<<"ERROR! Incorrect input directory adress!\n";
        return false;
    }
    return true;
}
int main(int argc, char *argv[]){
    int br = 0;
    namespace bfs = boost::filesystem;
    namespace logging = boost::log;
    namespace keywords = boost::log::keywords;
    namespace src = boost::log::sources;
    string input_dir = "C:/test/";
    Mat im;
    if (checkInputDir (input_dir)!=true)
    {
        return false;
    }
    if (input_dir.compare(input_dir.length()-1,1,"/")!=0)
    {
        input_dir+="/";
    }
    namedWindow("MYWINDOW", CV_WINDOW_AUTOSIZE);
    string intToString;
    string dir_direct;
    string dir_name;
    bfs::path* dir;
    for(int i = 0; i < 10; i++){
        dir_direct = input_dir;
        intToString = to_string(i);
        dir = new bfs::path(dir_direct+=intToString);
        bfs::create_directory(*dir);
        delete dir;
    }
    for(bfs::recursive_directory_iterator rdib(input_dir), rdie; rdib != rdie; ++rdib)
    {
        //cout<<"\n"<<rdib->path().filename().string();
        im=imread(input_dir+rdib->path().filename().string(), CV_LOAD_IMAGE_UNCHANGED);
        if (im.empty())
        {
            //cout<<" is not an image";
            //BOOST_LOG_TRIVIAL(trace)<<"Found file "<<rdib->path().filename().string()<<". It is not an image.";
           // qDebug() << "It is not an image";
        }
        else
        {
            imshow("MYWINDOW", im);
            waitKey(1);
            cout << "directory name";
            cin >> br;
            intToString = to_string(br);
            dir_name = input_dir + intToString + "/" + rdib->path().filename().string();
            cout << " " << dir_name << " ";
            imwrite(dir_name,im);
        }
        cout<<"\n\n";
    }
     cvvDestroyWindow("MYWINDOW");
}
